# esp32-temperature


The project is based on [esp32-ds18b20-example](https://github.com/DavidAntliff/esp32-ds18b20-example) that uses 

* 1-wire communication library [esp32-owb](https://github.com/DavidAntliff/esp32-owb)
* ds18b20 protocol library [esp32-ds18b20](https://github.com/DavidAntliff/esp32-ds18b20)
 
The idea of the project is to:

* See how/if sensor works fine with ESP32 using only dumps
* To add relay functionality and hysteresis
* To add web service controllable from outside using dyndns

# Version 0.1

To the main function of temp measurement, this system adds also relay controlable functionality.
Relay configuration is (in menuconfig) to:

* Be controlled by specific temp sensor and 
* Have defined hysteresis parameters for relay

# Version 0.2

This version adds web server that can be used for reading temperature parameters from ESP32.