/*
 * MIT License
 *
 * Copyright (c) 2017 David Antliff
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <inttypes.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "ssd1306.h"
#include "fonts.h"
#include "driver/gpio.h"
#include <stdio.h>

// Uncomment to enable static (stack-based) allocation of instances and avoid malloc/free.
//#define USE_STATIC 1

#include "owb.h"
#include "owb_rmt.h"
#include "ds18b20.h"

//includes from webserver
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "freertos/portmacro.h"
#include "freertos/event_groups.h"
#include "tcpip_adapter.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/api.h"
#include "lwip/err.h"
#include "string.h"
#include "cJSON.h"

static EventGroupHandle_t wifi_event_group;
const int CONNECTED_BIT = BIT0;

#define delay(ms) (vTaskDelay(ms/portTICK_RATE_MS))

#define GPIO_DS18B20_0       (CONFIG_ONE_WIRE_GPIO)
#define MAX_DEVICES          (8)
#define DS18B20_RESOLUTION   (DS18B20_RESOLUTION_12_BIT)
#define SAMPLE_PERIOD        (1000)   // milliseconds

//DON'T USE GPIO0, 2 FOR GPIO IT WILL MESS UP THE FLASH BOOT
#define GPIO_RELAY_1		 14
#define GPIO_OUTPUT_PIN_SEL  (1ULL<<GPIO_RELAY_1)

static const char * TAG = "app_main";

//webserver part webpage
const static char http_html_hdr[] =
		"HTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n";
char http_index_hml[] =
		"<!DOCTYPE html>"
				"<html>\n"
				"<head>\n"
				"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
				"<style type=\"text/css\">\n"
				"html, body, iframe { margin: 0; padding: 0; height: 100%; }\n"
				"iframe { display: block; width: 100%; border: none; }\n"
				"</style>\n"
				"<title>HELLO ESP32</title>\n"
				"<meta http-equiv=\"refresh\" content=\"5\">\n"
				"</head>\n"
				"<body>\n"
				"<h1>Prikaz temperature s ESP32</h1>\n"
				"<p>Senzor 1:       </p>\n"
				"<p>Senzor 2:       </p>\n"
				"<p>Senzor 3:       </p>\n"
				"</body>\n"
				"</html>\n";

static esp_err_t event_handler(void *ctx, system_event_t *event) {
	switch (event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;
	case SYSTEM_EVENT_STA_GOT_IP:
		xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
		printf("got ip\n");
		printf("ip: " IPSTR "\n", IP2STR(&event->event_info.got_ip.ip_info.ip));
		printf("netmask: " IPSTR "\n",
				IP2STR(&event->event_info.got_ip.ip_info.netmask));
		printf("gw: " IPSTR "\n", IP2STR(&event->event_info.got_ip.ip_info.gw));
		printf("\n");
		fflush(stdout);
		break;
	case SYSTEM_EVENT_STA_DISCONNECTED:
		/* This is a workaround as ESP32 WiFi libs don't currently
		 auto-reassociate. */
		esp_wifi_connect();
		xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
		break;
	default:
		break;
	}
	return ESP_OK;
}

static void initialise_wifi(void) {
	tcpip_adapter_init();
	wifi_event_group = xEventGroupCreate();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	wifi_config_t sta_config = {
			.sta = {
					.ssid = "Vedran",
					.password = "",
					.bssid_set = false
			}
	};

	ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
	ESP_ERROR_CHECK(esp_wifi_start());
}

static void http_server_netconn_serve(struct netconn *conn) {
	struct netbuf *inbuf;
	char *buf;
	u16_t buflen;
	err_t err;

	/* Read the data from the port, blocking if nothing yet there.
	 We assume the request (the part we care about) is in one netbuf */
	err = netconn_recv(conn, &inbuf);

	if (err == ERR_OK) {
		netbuf_data(inbuf, (void**) &buf, &buflen);

		/* Is this an HTTP GET command? (only check the first 5 chars, since
		 there are other formats for GET, and we're keeping it very simple )*/
		printf("buffer = %s \n", buf);
		if (buflen >= 5 && buf[0] == 'G' && buf[1] == 'E' && buf[2] == 'T'
				&& buf[3] == ' ' && buf[4] == '/') {
			printf("buf[5] = %c\n", buf[5]);
			/* Send the HTML header
			 * subtract 1 from the size, since we dont send the \0 in the string
			 * NETCONN_NOCOPY: our data is const static, so no need to copy it
			 */

			netconn_write(conn, http_html_hdr, sizeof(http_html_hdr) - 1,
					NETCONN_NOCOPY);

			netconn_write(conn, http_index_hml, sizeof(http_index_hml) - 1,
					NETCONN_NOCOPY);
		}

	}
	/* Close the connection (server closes in HTTP) */
	netconn_close(conn);

	/* Delete the buffer (netconn_recv gives us ownership,
	 so we have to make sure to deallocate the buffer) */
	netbuf_delete(inbuf);
}

static void task_http_server(void *pvParameters) {
	struct netconn *conn, *newconn;
	err_t err;
	conn = netconn_new(NETCONN_TCP);
	netconn_bind(conn, NULL, 80);
	netconn_listen(conn);
	TickType_t last_wake_time = xTaskGetTickCount();
	do {
		last_wake_time = xTaskGetTickCount();
		err = netconn_accept(conn, &newconn);
		if (err == ERR_OK) {
			http_server_netconn_serve(newconn);
			netconn_delete(newconn);
		}
		vTaskDelayUntil(&last_wake_time,
						SAMPLE_PERIOD / portTICK_PERIOD_MS);
	} while (err == ERR_OK);
	ESP_LOGE(TAG, "Error in HTTP server");
	netconn_close(conn);
	netconn_delete(conn);

}

static void task_sensors_reading(void *pvParameters) {
	vTaskDelay(3000 / portTICK_PERIOD_MS);
	char temperature[3][5];
	int i = 0;
	OneWireBus * owb;
	owb_rmt_driver_info rmt_driver_info;
	owb = owb_rmt_initialize(&rmt_driver_info, GPIO_DS18B20_0, RMT_CHANNEL_1,
			RMT_CHANNEL_0);

	owb_use_crc(owb, true);              // enable CRC check for ROM code

	// Find all connected devices
	ESP_LOGI(TAG, "Find devices:\n");
	OneWireBus_ROMCode device_rom_codes[MAX_DEVICES] = { 0 };
	int num_devices = 0;
	OneWireBus_SearchState search_state = { 0 };
	bool found = false;
	owb_search_first(owb, &search_state, &found);
	while (found) {
		char rom_code_s[17];
		owb_string_from_rom_code(search_state.rom_code, rom_code_s,
				sizeof(rom_code_s));
		ESP_LOGI(TAG, "Device %d : %s\n", num_devices, rom_code_s);
		device_rom_codes[num_devices] = search_state.rom_code;
		++num_devices;
		owb_search_next(owb, &search_state, &found);
	}

	ESP_LOGI(TAG, "Found %d devices\n", num_devices);

	DS18B20_Info * devices[MAX_DEVICES] = { 0 };

	for (int i = 0; i < num_devices; ++i) {
		DS18B20_Info * ds18b20_info = ds18b20_malloc();  // heap allocation
		devices[i] = ds18b20_info;

		if (num_devices == 1) {
			printf("Single device optimisations enabled\n");
			ds18b20_init_solo(ds18b20_info, owb);      // only one device on bus
		} else {
			ds18b20_init(ds18b20_info, owb, device_rom_codes[i]); // associate with bus and device
		}
		ds18b20_use_crc(ds18b20_info, true); // enable CRC check for temperature readings
		ds18b20_set_resolution(ds18b20_info, DS18B20_RESOLUTION);
	}

	ESP_LOGI(TAG, "Iniitaliation of ds18b20 passed");

	// Read temperatures more efficiently by starting conversions on all devices at the same time
	int errors_count[MAX_DEVICES] = { 0 };
	int sample_count = 0;

	if (num_devices > 0) {
		ESP_LOGI(TAG, "Reading temperatures");
		TickType_t last_wake_time = xTaskGetTickCount();

		while (1) {
			last_wake_time = xTaskGetTickCount();

			//questionable function...check it later
			ds18b20_convert_all(owb);

			// In this application all devices use the same resolution,
			// so use the first device to determine the delay
			ds18b20_wait_for_conversion(devices[0]);

			// Read the results immediately after conversion otherwise it may fail
			// (using printf before reading may take too long)
			float readings[MAX_DEVICES] = { 0 };
			DS18B20_ERROR errors[MAX_DEVICES] = { 0 };

			for (int i = 0; i < num_devices; ++i) {
				errors[i] = ds18b20_read_temp(devices[i], &readings[i]);
			}

			// Print results in a separate loop, after all have been read
			printf("\nTemperature readings (degrees C): sample %d\n",
					++sample_count);
			for (int i = 0; i < num_devices; ++i) {
				if (errors[i] != DS18B20_OK) {
					++errors_count[i];
				}

				printf("  %d: %.1f    %d errors\n", i, readings[i],
						errors_count[i]);
				ESP_LOGE(TAG, "Error while reading sensor %d", i);
			}
			for (i = 0; i < num_devices; ++i) {
				sprintf(temperature[i], "%2.1f", readings[i]);
				int j=0;
				for (j=0;j<4;++j)
				{
					http_index_hml[382+i*24+j] = (char)temperature[i][j];
				}
				if (i == 0) {
					ssd1306_draw_string(0, 0, 28 + i * 12, "Sensor 0:", 1, 0);
				} else if (i == 1) {
					ssd1306_draw_string(0, 0, 28 + i * 12, "Sensor 1:", 1, 0);
				} else if (i == 2) {
					ssd1306_draw_string(0, 0, 28 + i * 12, "Sensor 2:", 1, 0);
				}
				ssd1306_draw_string(0, 45, 28 + i * 12, temperature[i], 1, 0);
				ssd1306_refresh(0, true);
			}
			//turning relay on or off depending on the readings
			if (readings[2] > 30) {
				gpio_set_level(GPIO_RELAY_1, 0);
			} else if (readings[2] < 28) {
				gpio_set_level(GPIO_RELAY_1, 1);
			}

			vTaskDelayUntil(&last_wake_time,
			SAMPLE_PERIOD / portTICK_PERIOD_MS);
		}
	}

#ifndef USE_STATIC
	// clean up dynamically allocated data
	for (int i = 0; i < num_devices; ++i) {
		ds18b20_free(&devices[i]);
	}
	ESP_LOGI(TAG, "Buffers for ds18b20 released");
	//    owb_free(&owb);
#endif
}

static void initialise_oled(void) {
	//Initialise OLED display (ID=0, SCL=4, SDA=5)
	if (ssd1306_init(0, 4, 5)) {
		ESP_LOGI("OLED", "oled initialised");
		ssd1306_select_font(0, 1);
		ssd1306_draw_string(0, 0, 0, "OLED initialised", 1, 0);
		ssd1306_draw_string(0, 0, 15, "Current temperature: ", 1, 0);
		ssd1306_refresh(0, true);
	} else {
		ESP_LOGE("OLED", "oled init failed");
	}
}

static void initialise_relay_gpio(void) {
	//GPIO_14 output initialisation
	gpio_config_t io_conf;
	//disable interrupt
	io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
	//set as output mode
	io_conf.mode = GPIO_MODE_OUTPUT;
	//bit mask of the pins that you want to set,e.g.GPIO18/19
	io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
	//disable pull-down mode
	io_conf.pull_down_en = 0;
	//disable pull-up mode
	io_conf.pull_up_en = 0;
	//configure GPIO with the given settings
	gpio_config(&io_conf);
	gpio_set_level(GPIO_RELAY_1, 1);
}

void app_main() {
	//change it to ESP_LOG_DEBUG maybe
//	esp_log_level_set("*", ESP_LOG_INFO);
	// Stable readings require a brief period before communication
	vTaskDelay(2000.0 / portTICK_PERIOD_MS);

	//oled initialisation
	initialise_oled();
	ESP_LOGI(TAG, "Passed OLED init");

	//GPIO_14 output initialisation
	initialise_relay_gpio();
	ESP_LOGI(TAG, "Passed RELAY init");

	nvs_flash_init();
	ESP_LOGI(TAG, "Passed nvs_flash init");
	initialise_wifi();
	ESP_LOGI(TAG, "Passed WIFI init");

	xTaskCreate(&task_sensors_reading, "sensors_reading", 2048, NULL, 5, NULL);
	ESP_LOGI(TAG, "Passed sensor task creation");
	xTaskCreate(&task_http_server, "http_server", 2048, NULL, 5, NULL);
	ESP_LOGI(TAG, "Passed http_server task creation");
}
